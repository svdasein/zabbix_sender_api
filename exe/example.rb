#!/usr/bin/env ruby
require 'zabbix_sender_api'

sender = Zabbix::Sender::Pipe.new

rawdata = { :myFirstKey => 0, :mySecondKey => 100 }

data = Zabbix::Sender::Batch.new

rawdata.each_pair {|key,value|
  data.addItemData(key: key,value: value)
}

disco = Zabbix::Sender::Discovery.new(key: 'discoveryRuleKey')

disco.add_entity(:SOMEUSEFULVALUE => 'aValue', :ANOTHERONE => 'somethingElse')

data.addDiscovery(disco)

puts data.to_senderline

sender.sendBatchAtomic(data)
