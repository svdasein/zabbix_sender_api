require "zabbix_sender_api/version"
require "zabbix_sender_api/api"
require "json"

module ZabbixSenderApi
  class Error < StandardError; end
  # Note: You can call this again yourself with a path: parameter
  # to override the default list if need be.
  Zabbix::AgentConfiguration.initialize
end
