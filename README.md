# zabbix_sender_api [![Gem Version](https://badge.fury.io/rb/zabbix_sender_api.svg)](https://badge.fury.io/rb/zabbix_sender_api)

This gem provides a model for assembling and sending data to Zabbix via its sender/trapper mechanism.  It works both with the zabbix_sender command line utility, or by itself using a socket connection to a trapper port direct from Ruby.

**Detailed documentation for this library is [>>HERE<<](https://svdasein.gitlab.io/zabbix_sender_api)**

**Source repository is [>>HERE<<](https://gitlab.com/svdasein/zabbix_sender_api)**

Look at [the source for the zfstozab gem](https://gitlab.com/svdasein/zfstozab/-/blob/master/exe/zabbix-zfs.rb) for a practical example how to use this api.

zabbix_sender_api can send data to zabbix via one of two included connection types:

 * zabbix_sender (this was the original and for quite some time only mode supported)
 * TCP socket connection directly to a Zabbix "trapper" port (as of vers 1.1.0)

If you need to interact with the Zabbix REST API, you might also be interested in [zabbix-api-simple](https://svdasein.gitlab.io/zabbix-api/)


## Installation

    $ gem install zabbix_sender_api

## Usage

### Synopsis
```ruby
#!/usr/bin/env ruby
require 'zabbix_sender_api'

sender = Zabbix::Sender::Pipe.new

rawdata = { :myFirstKey => 0, :mySecondKey => 100 }

data = Zabbix::Sender::Batch.new

rawdata.each_pair {|key,value|
  data.addItemData(key: key,value: value)
}

sender.sendBatchAtomic(data)
```

The above will execute zabbix_sender for you and send data into it as described above.

Alternatively you can just

```ruby
puts data.to_senderline
```
... and do whatever you want w/ the stdout


To do low level discovery:

```ruby
disco = Zabbix::Sender::Discovery.new(key: 'discoveryRuleKey')

disco.add_entity(:SOMEUSEFULVALUE => 'aValue', :ANOTHERONE => 'somethingElse')

data.addDiscovery(disco)

```

### Under the hood

#### Zabbix::Sender::Pipe method:
The zabbix-sender cli utility provides a number of methods by which to insert data into zabbix. 

 * zabbix-sender ... -s zabbixHostName -k keyName -o value (one k-v pair at a time)
 * zabbix-sender ... -i - (series of kv pairs from stdin using zabbix-sender start time as timestamp)
 * zabbix-sender ... -T -i - (series of kv pairs with their own embedded timestamps from stdin)

In the latter two cases, the zabbix host name (the name of the host that zabbix is monitoring) is embedded
in the stdin data.

In all cases it is presumed that the zabbix server or proxy to which data should be sent is specified.  This can
be done either by specifying it explicitly with the -z switch, or indirectly by pointing it to a zabbix sender configuration
file with the -c switch.  If you let zabbix_sender_api handle executing zabbix_sender, the target will be specified on the
command line via the -z switch.


zabbix_sender_api utilizes the -T -i - form, so the generated data lines look like this:

```
"theHostBeingMonitored" myFirstKey 1551719379 0
"theHostBeingMonitored" mySecondKey 1551719379 100
```

The above lines will send data to two items associated with the host theHostBeingMonitored: myFirstKey gets the value 0, and mySecondKey gets the value 100.  In both cases the
time stamp is identical but it need not be (see the exe/example.rb for specifics).


Low level discovery (LLD) is also possible with zabbix-sender; the format of the stdin data is not so pretty but zabbix_sender_api handles all that:

```
"theHostBeingMonitored" discoveryRuleKey 1551719797 {"data":[{"{#SOMEUSEFULVALUE}":"aValue","{#ANOTHERONE}":"somethingElse"}]}
```
The above line sends an LLD discovery structure (formatted as json) to the [discovery rule](https://www.zabbix.com/documentation/4.0/manual/discovery/low_level_discovery#discovery_rule) whose key is discoveryRuleKey.  It describes one entity by passing the macro values
{#SOMEUSEFULVALUE} and {#ANOTHERONE} to the discovery rule.  These '[lld macros](https://www.zabbix.com/documentation/4.0/manual/config/macros/lld_macros)' are available for use in item,trigger, and graph prototypes.


If you wished to use the above lld to actually do some discovery, you'd set things up in zabbix roughly like this:

![Discovery rule configuration](images/Spectacle.Z29721.png)
![Item prototype configuration](images/Spectacle.l29721.png )

#### Zabbix::Sender::Socket method:

You can switch between using the Pipe(zabbix_sender) method and the Socket(direct to zabbix via tcp socket) method very simply.  Just change:

```
sender = Zabbix::Sender::Pipe.new
```
to
```
sender = Zabbix::Sender::Socket.new
```

If you were specifiying a path to zabbix_sender using the Pipe method, remove that.  If you're using a socket other than 10051 on your server/proxy,  you'll want to add the port: option

The socket method doesn't support sending multiple batches between flushes, so you should just use sendBatchAtomic(aBatch) with the Socket method.

You don't have to change anything else - the rest of zabbix_sender_api works exactly the same with the Socket method, save for that it's arguably more efficient with system resources.  The only drawback might be if zabbix changes the sender protocol.  At that point until this library is update to support the change you can revert to the Pipe/zabbix_sender method if needed (if they change the command line switches radically there, changes will be required in this library as well).
