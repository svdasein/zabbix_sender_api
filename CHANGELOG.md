# 1.1.4
- added support for nanosecond timestamps
# 1.1.0
- added native (socket based) sender logic (Zabbix::Sender::Socket).
- changed Zabbix::Sender::Pipe's open method from popen to Open3::popen3 to allow capture of stdout stderf and exitA
- the Socket and Pipe .flush method returns status, sendBatchAtomic relays it from its internal call to flush
# 1.1.1
- added ability to override Zabbix::AgentConfiguration search path
- cleaned up a few issues + the docs
# 1.1.2
- This is just a documentation fix.  rubydoc.info wasn't generating docs from the gem properly so docs are now in a gitlab "pages" site.
# 1.1.3
- BUG fix for Discovery when using Socket mode.
