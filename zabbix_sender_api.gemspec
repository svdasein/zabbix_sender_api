require_relative 'lib/zabbix_sender_api/version'

Gem::Specification.new do |spec|
  spec.name          = "zabbix_sender_api"
  spec.version       = ZabbixSenderApi::VERSION
  spec.authors       = ["Dave Parker"]
  spec.email         = ["daveparker01@gmail.com"]

  spec.summary       = %q{Ruby library that greatly simplifies sending data to Zabbix via the sender/trapper protocol}
  spec.description   = %q{This gem implements an api that abstracts the zabbix sender/trapper mechanism. It saves tons of time when you're cranking out custom polling logic}
  spec.homepage      = "https://svdasein.gitlab.io/zabbix_sender_api"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/svdasein/zabbix_sender_api"
  spec.metadata["changelog_uri"] = "https://gitlab.com/svdasein/zabbix_sender_api/-/blob/master/CHANGELOG.md"


  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
