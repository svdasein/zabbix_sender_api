```
root@zabbix-server:~# /opt/zabbix-server/bin/zabbix_sender -h
usage:
  zabbix_sender [-v] -z server [-p port] [-I IP-address] -s host -k key
                -o value
  zabbix_sender [-v] -z server [-p port] [-I IP-address] [-s host] [-T] [-r]
                -i input-file
  zabbix_sender [-v] -c config-file [-z server] [-p port] [-I IP-address]
                [-s host] -k key -o value
  zabbix_sender [-v] -c config-file [-z server] [-p port] [-I IP-address]
                [-s host] [-T] [-r] -i input-file
  zabbix_sender -h
  zabbix_sender -V

Utility for sending monitoring data to Zabbix server or proxy.

General options:
  -c --config config-file    Path to Zabbix agentd configuration file

  -z --zabbix-server server  Hostname or IP address of Zabbix server or proxy
                             to send data to. When used together with --config,
                             overrides the first entry of "ServerActive"
                             parameter specified in agentd configuration file

  -p --port port             Specify port number of trapper process of Zabbix
                             server or proxy. When used together with --config,
                             overrides the port of the first entry of
                             "ServerActive" parameter specified in agentd
                             configuration file (default: 10051)

  -I --source-address IP-address   Specify source IP address. When used
                             together with --config, overrides "SourceIP"
                             parameter specified in agentd configuration file

  -s --host host             Specify host name the item belongs to (as
                             registered in Zabbix frontend). Host IP address
                             and DNS name will not work. When used together
                             with --config, overrides "Hostname" parameter
                             specified in agentd configuration file

  -k --key key               Specify item key
  -o --value value           Specify item value

  -i --input-file input-file   Load values from input file. Specify - for
                             standard input. Each line of file contains
                             whitespace delimited: <host> <key> <value>.
                             Specify - in <host> to use hostname from
                             configuration file or --host argument

  -T --with-timestamps       Each line of file contains whitespace delimited:
                             <host> <key> <timestamp> <value>. This can be used
                             with --input-file option. Timestamp should be
                             specified in Unix timestamp format

  -r --real-time             Send metrics one by one as soon as they are
                             received. This can be used when reading from
                             standard input

  -v --verbose               Verbose mode, -vv for more details

  -h --help                  Display this help message
  -V --version               Display version number

TLS connection options:
  Not available. This Zabbix sender was compiled without TLS support

Example(s):
  zabbix_sender -z 127.0.0.1 -s "Linux DB3" -k db.connections -o 43

Report bugs to: <https://support.zabbix.com>
Zabbix home page: <http://www.zabbix.com>
Documentation: <https://www.zabbix.com/documentation>
```